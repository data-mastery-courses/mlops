# Exercise - Target & Concept drift
We finally received the new labels from our Subject Matter Experts, we would like to see whether there is any data drift.

## Measure target drift
- Investigate target drift using deepchecks to check whether the data in `labels.parquet` has drifted compared to the train set labels `y`.

## Measure concept drift
- Use the original model to predict labels based on the new production data `digits.parquet` then log the accuracy, precision and f1 score to Azure ML.
- Compare the performance metrics between the original v.s. the new dataset.
- Create new script `code/retrain_model.py` to train a new ML model on the combined dataset (train `X` + new production data `digits.parquet`) and log the same performance metrics to Azure ML.
- Compare the performance metrics of the original v.s. the new model in Azure ML.
- (Optional) Check whether your model still works well for the original data. Hint: `model.predict(X_digits_old)`
