# Exercise - Continuous Delivery (gitlab)
- Create a new train_model and deploy_model script to do these steps in gitlab
- Make sure you use ServicePrincipalAuthentication instead of InteractiveLoginAuthentication (credentials will be handed over: make sure to store them as masked (& not protected) variables in the gitlab portal)
- Update .gitlab-ci.yml, add a train and a deploy step
- (Optional) Set up "rules" for these steps so that:
  - The deploy step only runs when the pipeline is executed in the `main` branch
  - The train step only runs when the pipeline is executed in the `main` branch or in a PR to the main branch
  - The train_model Python script only registers the model when the script is executed in the `main` branch
  - Hint: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
