import joblib
import pandas as pd
from deepchecks.tabular import Dataset
from deepchecks.tabular.checks import MultivariateDrift
from fastapi import FastAPI
from pydantic import BaseModel
from sklearn import datasets
from xgboost.sklearn import XGBClassifier

# uvicorn code.fastapi_app_drift_detection:app --host 0.0.0.0 --port 6789 --reload

reference_df = datasets.load_digits(as_frame=True).data


model: XGBClassifier = joblib.load("model.joblib")
app = FastAPI()


class DigitDataset(BaseModel):
    """Dataset of digits, datatypes are validated for each request."""

    data: list[dict[str, float]]
    ignore_drift = False


@app.post("/score")
async def score(dataset: DigitDataset):
    """Create a prediction"""
    df = pd.DataFrame(dataset.data)

    if not dataset.ignore_drift and (drift := calculate_multivariate_drift(df)) > 0.5:
        return f"Error: {int(drift*100)}% multivariate drift detected"

    y_pred = model.predict(df)
    return y_pred.tolist()


def calculate_multivariate_drift(df: pd.DataFrame):
    """Check whether df contains data drift compared to reference df."""
    ds_ref = Dataset(reference_df, cat_features=[])
    ds_new = Dataset(df, cat_features=[])
    check = MultivariateDrift()
    result = check.run(train_dataset=ds_ref, test_dataset=ds_new)

    # Create metrics dict with drift score
    return result.value["domain_classifier_drift_score"]
