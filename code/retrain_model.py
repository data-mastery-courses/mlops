# %%
import os

import aml_utils
import mlflow
import pandas as pd
from sklearn import datasets, metrics
from sklearn.model_selection import train_test_split
from xgboost.sklearn import XGBClassifier

digits_old = datasets.load_digits(as_frame=True)
new_df = pd.read_parquet("digits.parquet")
labels = pd.read_parquet("labels.parquet")
X = pd.concat([digits_old.data, new_df])
y = pd.concat([digits_old.target, labels.target])
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# %%
aml_utils.connect()

# Set experiment name to user specific name to start logging user specific drift runs
mlflow.set_experiment(os.environ["EXPERIMENT_NAME"])
# Start training run logged to mlflow
with mlflow.start_run():
    # set xgboost params
    params = {
        "max_depth": 4,  # the maximum depth of each tree
        "eta": 0.15,  # the training step for each iteration
        "objective": "multi:softprob",  # error evaluation for multiclass training
        "eval_metric": "mlogloss",
    }
    mlflow.log_params(params)

    # training and testing
    model = XGBClassifier(**params)
    _ = model.fit(X_train, y_train)
    y_pred = model.predict(X_test)

    # log results to mlflow
    metrics_results = {
        "precision": metrics.precision_score(y_test, y_pred, average="macro"),
        "accuracy": metrics.accuracy_score(y_test, y_pred),
        "f1": metrics.f1_score(y_test, y_pred, average="macro"),
    }
    print("New model performance on old and new data:")
    print(metrics_results)
    mlflow.log_metrics(metrics_results)

    # Set ready_for_reg to true when model tuning is finished
    ready_for_reg = False
    mlflow.set_tag("ready_for_reg", ready_for_reg)

    # Save the model to the outputs directory for capture
    mlflow.sklearn.log_model(
        sk_model=model,
        artifact_path="model",
        registered_model_name=os.environ["MODEL_NAME"] if ready_for_reg else None,
        extra_pip_requirements="extra_deploy_requirements.txt",
    )

    # (Optional) Retrained model still works?
    y_pred = model.predict(digits_old.data)
    y_test = digits_old.target

    # Print metrics
    metrics_new_model_old_data = {
        "precision": metrics.precision_score(y_test, y_pred, average="macro"),
        "accuracy": metrics.accuracy_score(y_test, y_pred),
        "f1": metrics.f1_score(y_test, y_pred, average="macro"),
    }
    print("New model performance on old data:")
    print(metrics_new_model_old_data)
# %%
