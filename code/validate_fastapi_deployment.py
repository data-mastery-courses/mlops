# %%
import pandas as pd
import requests
from sklearn import datasets
from sklearn.model_selection import train_test_split

scoring_uri = "http://0.0.0.0:6789/score"
X, y = datasets.load_digits(as_frame=True, return_X_y=True)
# X = pd.read_parquet("digits.parquet")

# %%
_, X_test, _, y_test = train_test_split(X, y, test_size=0.05, random_state=42)
input_data = X_test.to_dict(orient="records")

resp = requests.post(scoring_uri, json={"data": input_data})

print("POST to url", scoring_uri)
print("Labels y_test:", y_test.values)
print("Prediction endpoint:", resp.text)

# %%
